let trainer = {
	name: "Misty",
	age: "23",
	address:{
		city: "Cerulean Town",
		country: "Kanto",
	},
	friends: ["Ash", "Brock", "May"],
	pokemons: ["Togepi", "Horsea", "Starmie", "Goldeen", "Psyduck"],
	catch: function(pokeBall){
			if(this.pokemons.length == 6){
				console.log("A trainer should only have 6 pokemons to carry.")
			}
			else{
				console.log("Gotcha")
				this.pokemons.push(pokeBall)
			}
	},
	release: function(){
		if(this.pokemons.length == 0){
			console.log("You have no more pokemons! Catch one first.")
		}
		else{
			console.log("Pokemon is released!")
			this.pokemons.pop()
		}
	}
}

function stats(name, type, level){
	this.name = name
	this.type = type
	this.level = level
	this.hp = 3 * level
	this.atk = 2.5 * level
	this.def = 2 * level
	this.isFainted = false
	console.log(this.name, this.type, this.level, this.hp, this.atk, this.def)
	this.tackle = function(pok){
		console.log(`${this.name} tackled ${pok.name}!`)
		if(this.isFainted == false){
			let result1 = this.hp - pok.atk
			let result2 = pok.hp - this.atk
			if(result1 > result2){
				this.faint1(pok)
			}
			else
			{
				this.faint()
			}
		}
	}
	this.faint = function(){
		alert(`${this.name} has fainted.`)
		this.isFainted = true
	}	
	this.faint1 = function(pok){
		alert(`${pok.name} has fainted.`)
		this.isFainted = true
	}
}

let battle1 = new stats("Charizard", "Fire", 47)
let battle2 = new stats("Blastoise", "Water", 40)
